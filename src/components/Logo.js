import React from 'react'
import logo from '../assets/images/logo.png'
const Logo = () => {
  return (
    <>
      <img width='50px' height='50px' src={logo} alt='' />
    </>
  )
}

export default Logo
