import React from 'react'
import '../assets/styles/btnleftNav.css'
const BtnLeftNav = ({ titre, icons, onclick }) => {
  return (
    <button onClick={onclick} className='boxBtn btn'>
      <div>
        <i className={icons} />
      </div>
      {titre}
    </button>

  )
}

export default BtnLeftNav
