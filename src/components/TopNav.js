import React from 'react'
import Logo from './Logo'
import '../assets/styles/topNav.css'
import { useDispatch, useSelector } from 'react-redux'

const TopNav = () => {
  const data = useSelector(state => state.index.data)
  const dispatch = useDispatch()
  const logout = () => {
    dispatch({ type: 'DISCONNECT' })
    window.location.href = '/'
  }
  return (
    <div className='container_top_Nav'>
      <Logo />
      <div className=' box '>
        <p>{data.name}{data.username}</p>
        <div>
          <button onClick={logout} className='btn btn-outline-light'>deconnexion</button>
        </div>
      </div>
    </div>
  )
}

export default TopNav
