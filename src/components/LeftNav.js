import React from 'react'
import BtnLeftNav from './BtnLeftNav'
import '../assets/styles/leftNav.css'
import { Link } from 'react-router-dom'

const LeftNav = () => {
  return (
    <div className='container_left_nav'>
      <Link to='/identityPatient'>
        <BtnLeftNav titre='1 ére visite' icons='fas fa-user-plus' />
      </Link>
      <Link to='/screenB'>
        <BtnLeftNav titre='Adaptation' icons='fas fa-user-md' />
      </Link>
      <Link to='/screenC'>
        <BtnLeftNav titre='Essais' icons='fas fa-file-medical' />
      </Link>
      <Link to='/screenD'>
        <BtnLeftNav titre='Appareillés' icons='fas fa-user-nurse' />
      </Link>
      <Link to='/screenE'>
        <BtnLeftNav titre='Relance' icons='far fa-clipboard' />
      </Link>
    </div>
  )
}

export default LeftNav
