import React from 'react'

// import { Container } from './styles';

const CheckBox = ({ checked, title, key, onChange }) => {
  return (
    <div key={key} className='form-check'>
      <input onChange={onChange} checked={checked} type='checkbox' className='form-check-input' id='exampleCheck1' />
      <label className='form-check-label' htmlFor='exampleCheck1'>{title}</label>
    </div>
  )
}
export default CheckBox
