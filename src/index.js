import React from 'react'
import ReactDOM from 'react-dom'
import App from './screens/App'
import {BrowserRouter} from 'react-router-dom'
import * as serviceWorker from './serviceWorker'
import {createStore, combineReducers} from 'redux'
import {Provider} from 'react-redux'
import index from './store/reducers/index'
import signinReducers from './store/reducers/signinReducers'
import identityPatientReducers from './store/reducers/identityPatientReducers'

const store = createStore(
    combineReducers({
        index,
        signinReducers,
        identityPatientReducers
    }), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)
ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <App/>
        </BrowserRouter>
    </Provider>
    , document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister()
