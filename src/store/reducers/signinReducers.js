const initialState = {
  isConnected: false
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case 'IS_LOGIN':
      return { ...state, isConnected: state.isConnected = true }
    case 'DISCONNECT':
      return { ...state, isConnected: state.isConnected = false }
    default:
      return state
  }
}
