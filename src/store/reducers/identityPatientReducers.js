const initialState = {
  user: {
    firstname: null,
    lastname: null,
    gender: null
  }

}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case 'INSCRIPTION':
      return { ...state, user: { ...state.user, ...payload } }
    default:
      return state
  }
}
