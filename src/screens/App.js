import React from 'react'
import PublicRouter from '../routeur/PublicRouteur'
import Home from './Home'
import Signin from './Signin'
import { useSelector } from 'react-redux'
import { Switch, Route, withRouter } from 'react-router-dom'
function App () {
  const isConnected = useSelector(state => state.signinReducers.isConnected)
  return (
    <div>
      <>
        {isConnected && <Home />}
        <Route exact path='/' component={Signin} />
      </>
    </div>
  )
}

export default App
