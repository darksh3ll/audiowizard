import React, { useState } from 'react'
import { Redirect } from 'react-router-dom'
import { useDispatch } from 'react-redux'
const IdentityPatient = () => {
  const [firstname, setFirstName] = useState('')
  const [lastname, setLastname] = useState('')
  const [redirect, setRedirect] = useState(false)
  const dispatch = useDispatch()

  const getForm = (e) => {
    e.preventDefault()
    if (lastname.length > 0 || firstname.length > 0) {
      dispatch({ type: 'INSCRIPTION', payload: { firstname, lastname } })
      setRedirect(true)
    } else {
      alert('please fill in the fields')
    }
  }

  if (redirect) {
    return <Redirect push to='/gender' />
  }

  return (
    <div>
      <div className='alert alert-success' role='alert'>
                can you indicate the patient's identity
      </div>
      <div>
        <form onSubmit={getForm}>
          <div className='form-group'>
            <label htmlFor='exampleInputEmail1'>Firstname</label>
            <input
              type='text'
              onChange={(e) => (setFirstName(e.target.value))}
              className='form-control'
              id='firstname'
              placeholder='firstname'
            />

          </div>
          <div className='form-group'>
            <label htmlFor='exampleInputPassword1'>lastname</label>
            <input
              type='text'
              className='form-control'
              id='exampleInputPassword1'
              placeholder='lastname'
              onChange={(e) => (setLastname(e.target.value))}
            />
          </div>
          <button type='submit' className='btn btn-outline-secondary'>Next</button>
        </form>

      </div>
    </div>

  )
}

export default IdentityPatient
