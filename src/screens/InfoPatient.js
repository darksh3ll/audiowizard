import React from 'react'
import { useSelector } from 'react-redux'

const InfoPatient = () => {
  const infoPatient = useSelector(state => state.identityPatientReducers.user)
  return (
    <div>
      <h1>
        {infoPatient.gender === 'Male' ? 'Monsieur' : 'Madame'}
        {' '}
        {infoPatient.firstname}
        {' '}
        {infoPatient.lastname}
      </h1>

    </div>
  )
}

export default InfoPatient
