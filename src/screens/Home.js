import React, { useEffect } from 'react'
import LeftNav from '../components/LeftNav'
import TopNav from '../components/TopNav'
import MainContainer from '../screens/MainContainer'
import axios from 'axios'
import {useDispatch} from 'react-redux'
import '../assets/styles/home.css'

const Home = () => {
  const dispatch = useDispatch()
  useEffect(() => {
    axios
      .get('https://jsonplaceholder.typicode.com/users/1')
      .then(result => dispatch({ type: 'FETCH_DATA', payload: result.data }))
  }, [dispatch])

  return (
    <>
      <TopNav />
      <div className='containerHome'>
        <LeftNav />
        <MainContainer />
      </div>
    </>
  )
};

export default Home
