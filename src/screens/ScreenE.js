import React from 'react'
import ReactLoading from 'react-loading'

const ScreenE = () => {
  return (
    <ReactLoading type='balls' color='#FF1493' height='20%' width='20%' />
  )
}

export default ScreenE
