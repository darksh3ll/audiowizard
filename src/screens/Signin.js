import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Redirect } from 'react-router-dom'
import '../assets/styles/signin.css'
const Signin = () => {
  const dispatch = useDispatch()
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const isConnected = useSelector(state => state.signinReducers.isConnected)
  const auth = (e) => {
    e.preventDefault()
    if (email === 'admin@admin.fr' || password === 'admin') {
      dispatch({ type: 'IS_LOGIN' })
    }
  }
  if (isConnected) {
    return <Redirect to='/home' />
  }
  return (
    <form onSubmit={auth} className=' container_signin'>
      <InputEmail setEmail={setEmail} />
      <InputPassword setPassword={setPassword} />
      <button type='submit' className='btn btn-primary'>Submit</button>
    </form>
  )
}

const InputEmail = ({ setEmail }) => (
  <div className='form-group'>
    <label htmlFor='exampleInputEmail1'>Email address</label>
    <input
      onChange={(e) => { setEmail(e.target.value) }}
      type='email'
      className='form-control'
      id='exampleInputEmail1'
      aria-describedby='emailHelp'
      placeholder='Enter email'
    />
  </div>
)

const InputPassword = ({ setPassword }) => (
  <div className='form-group'>
    <label htmlFor='exampleInputPassword1'>Password</label>
    <input
      onChange={(e) => { setPassword(e.target.value) }}
      type='password'
      className='form-control'
      id='exampleInputPassword1'
      placeholder='Password'
    />
  </div>
)
export default Signin
