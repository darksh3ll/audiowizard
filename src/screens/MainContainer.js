import React from 'react'
import { Switch, Route } from 'react-router-dom'
import '../assets/styles/mainContainer.css'
import IdentityPatient from './IdentityPatient'
import ScreenB from './ScreenB'
import ScreenC from './ScreenC'
import ScreenD from './ScreenD'
import ScreenE from './ScreenE'
import GenderPatient from './GenderPatient'
import InfoPatient from './InfoPatient'
export default function MainContainer () {
  return (
    <div className='container-fluid d-flex justify-content-center align-items-center'>
      <Switch>
        <Route exact path='/identityPatient' component={IdentityPatient} />
        <Route exact path='/gender' component={GenderPatient} />
        <Route exact path='/infoPatient' component={InfoPatient} />
        <Route exact path='/screenB' component={ScreenB} />
        <Route exact path='/screenC' component={ScreenC} />
        <Route exact path='/screenD' component={ScreenD} />
        <Route exact path='/screenE' component={ScreenE} />
      </Switch>
    </div>
  )
}
