import React from 'react'
import ReactLoading from 'react-loading'

const ScreenC = () => {
  return (
    <ReactLoading type='spin' color='#FF1493' height='20%' width='20%' />
  )
}

export default ScreenC
