import React from 'react'
import ReactLoading from 'react-loading'

const ScreenD = () => {
  return (
    <ReactLoading type='spinningBubbles' color='#FF1493' height='20%' width='20%' />
  )
}

export default ScreenD
