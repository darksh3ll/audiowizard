import React from 'react'
import { Link } from 'react-router-dom'
import { useDispatch } from 'react-redux'
const GenderPatient = () => {
  const dispatch = useDispatch()
  const selectedMale = () => {
    dispatch({ type: 'INSCRIPTION', payload: { gender: 'Male' } })
  }

  const selectedFemale = () => {
    dispatch({ type: 'INSCRIPTION', payload: { gender: 'Female' } })
  }

  return (
    <div>
      <AlerInfo />
      <div class='btn-group box' role='group' aria-label='Basic example'>
        <button onClick={selectedMale} type='button' class='btn btn-outline-primary btn-lg'>Male</button>
        <button onClick={selectedFemale} type='button' class='btn btn btn-outline-primary btn-lg'>Femelle</button>
      </div>
      <ButtonsRedirect />
    </div>
  )
}

const ButtonsRedirect = () => (
  <div className=' box buttons'>
    <Link to='/identityPatient'>
      <button type='submit' className='btn btn-outline-secondary '>Back</button>
    </Link>
    <Link to='/infoPatient'>
      <button type='submit' className='btn btn-outline-secondary '>Next</button>
    </Link>
  </div>
)

const AlerInfo = () => (
  <div
    className='alert alert-success'
    role='alert'
  >
  can you indicate patient gender
  </div>
)

export default GenderPatient
