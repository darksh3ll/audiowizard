import React from 'react'
import { Switch, Route,withRouter } from 'react-router-dom'
import Home from '../screens/Home'
import Signin from '../screens/Signin'
import { useSelector } from 'react-redux'

const PublicRouteur = () => {
  const isConnected = useSelector(state => state.signinReducers.isConnected)
  return (
    <>
        {
          isConnected &&
            <Route exact path='/home' component={Home} />
        }
        <Route exact path='/' component={Signin} />
    </>
  )
}

export default withRouter(PublicRouteur)
